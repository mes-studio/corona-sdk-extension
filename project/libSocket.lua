------------------------------------------------
--
-- v_0.0.3
-- Библиотека сокетов
--
------------------------------------------------

local socket = require('socket')
local URL = require('libUrl')
local M = {}

M.EventTypes = {
	NON_CONNECTION = 'NON_CONNECTION', -- Без соединения
	CANT_CONNECT_TO_SERVER = 'CANT_CONNECT_TO_SERVER', -- Невозможность подключится к серверу
	CONNECT = 'CONNECT', -- Подключение к серверу
	RECEIVE = 'RECEIVE', -- Получение данных с сервера
	LOSE_CONNECTION = 'LOSE_CONNECTION', -- Утеряно соединение
	TIMEOUT = 'TIMEOUT' -- Время ожидания истекло
}

-- Флаг инициализации
local alreadyInit = false

--	Словарь подключаемых сокетов
M.connects = {}

--[====[
	@type func
	@name M.init()
	@brief Первичная инициализация
--]====]
M.init = function()
	if not alreadyInit then
		--	Инициализировать можно только один раз
		alreadyInit = true

		Runtime:addEventListener('enterFrame', M.socketLoop)
	end
end

--[=====[
    @type func
    @name libSocket.create
    @param name:string Имя клиент-сокета
    @param host:string Имя хоста
	@param port:number Порт хоста
    @brief Создает сокет-клиент, но выполняет подключение
--]=====]
M.create = function(name, host, port)
	M.connects[name] = {
		host = host,
		port = port,
		state = M.EventTypes.NON_CONNECTION,
		events = {

		},
		sendMessageTime = nil,
		currTryReconnect = 0
	}

	M.connects[name].connection = M.connect(host, port)
end

--[=====[
    @type func
    @name libSocket.addEventListener
    @param eventType:string Тип события из libSocket.EventTypes
    @param handler:func Функция обратного вызова
    @brief Добавляет случашатель к указанному событию
--]=====]
M.addEventListener = function(name, eventType, func)
	if not M.connects[name].events[eventType] then
		M.connects[name].events[eventType] = {}
	end

	M.connects[name].events[eventType][#M.connects[name].events[eventType]+1] = func
end

--[====[
	@type func
	@name M.start()
	@brief Старт
--]====]
M.start = function()
	M.checkConnection()
end

--[=====[
    @type func
    @name libSocket.checkConnection
    @param name:string Имя клиент-сокета, если не указано, то будет выполнятся подключение всех добавленных сокет-клиентов
    @brief Обходит все/указанные сокет-клиенты и пытается установить подключение, после упеха/таймаута вызовет колбек
--]=====]
M.checkConnection = function(name, callback)
	local connects

	if name ~= nil then
		if not M.connects[name] then
			return false
		end

		connects = {}
		connects[name] = M.connects[name]
	else
		connects = M.connects
	end

	local period = 20
	local timeout = 1000
	local currTime = 0
	local timeoutTimer

	timeoutTimer = timer.performWithDelay(period, function()
			currTime = currTime + period

			for socketClientName, socketClient in pairs(M.connects) do
				if M.connects[socketClientName].state ~= M.EventTypes.CONNECT then
					local input, writeReady, err = socket.select(nil, {socketClient.connection}, 0)
					
					if writeReady[1] then
						local clinet, err, data = writeReady[1]:receive()
						print('SOCKET: ' .. data)

						M.connects[socketClientName].state = M.EventTypes.CONNECT

						local data = {
							connection = connection
						}

						if name ~= nil and callback ~= nil then
							timer.cancel(timeoutTimer)
							callback(socketClientName, M.EventTypes.CONNECT)
						else
							M.callEventListeners(socketClientName, M.EventTypes.CONNECT, data)
						end
					end
				end

				if currTime >= timeout then
					timer.cancel(timeoutTimer)

					if M.connects[socketClientName].state ~= M.EventTypes.CONNECT then
						print('SOCKET: timeout')
						socketClient.connection:close()

						if name ~= nil and callback ~= nil then
							callback(socketClientName, M.EventTypes.CANT_CONNECT_TO_SERVER)
						else
							M.callEventListeners(socketClientName, M.EventTypes.CANT_CONNECT_TO_SERVER)
						end
					end
				end
			end
		end, 0)
end

--[=====[
    @type func
    @name libSocket.socketLoop
    @brief Обходит все сокет-клиенты. При получении сообщения с сервера, вызывает обработчики события
--]=====]
M.socketLoop = function()
	for socketClientName, socketClient in pairs(M.connects) do
		if socketClient.state == M.EventTypes.CONNECT then
			local connection = socketClient.connection
			local input, output, err, client, data
			input, output, err = socket.select({connection}, {connection}, 0)

			if output[1] then
				clinet, err, data = output[1]:receive()

				if socketClient.sendMessageTime ~= nil  then
					if os.time() - socketClient.sendMessageTime > 2 then
						print('SOCKET: LOSE_CONNECTION')
						M.connects[socketClientName].state = M.EventTypes.LOSE_CONNECTION
						M.connects[socketClientName].sendMessageTime = nil

						M.closeConnection(socketClientName)
						M.callEventListeners(socketClientName, M.EventTypes.LOSE_CONNECTION)
					end
				end

				if data ~= '' then
					M.connects[socketClientName].sendMessageTime = nil

					local event = {
						message = data
					}

					M.callEventListeners(socketClientName, M.EventTypes.RECEIVE, event)
				end
			end
		end
	end
end

--[====[
	@type func
	@name M.tryReconnectHandler(name, result)
	@param name:string Имя сокет канала
	@param result:number Результат попытки переводключится
	@brief Листенер переподключение, когда пропала связь
--]====]
M.tryReconnectHandler = function(name, result)
	if M.connects[name].currTryReconnect < 20 and result == M.EventTypes.CANT_CONNECT_TO_SERVER then
		M.connects[name].currTryReconnect = M.connects[name].currTryReconnect + 1

		M.connects[name].connection:close()
		M.connects[name].connection:connect(M.connects[name].host, M.connects[name].port)

		M.checkConnection(name, M.tryReconnectHandler)
	else
		print('SOCKET: Successfully reconnect: ', M.connects[name].currTryReconnect);
		local currTryReconnectTmp = M.connects[name].currTryReconnect
		M.connects[name].currTryReconnect = 0

		if currTryReconnectTmp == 20 then
			M.callEventListeners(name, M.EventTypes.TIMEOUT, data)
		else
			M.callEventListeners(name, M.EventTypes.CONNECT, data)
		end
	end
end

--[====[
	@type func
	@name M.tryReconnect(name)
	@param name:string Имя сокет канала
	@brief Попытаться переводключится, когда было потеряна связь
--]====]
M.tryReconnect = function(name)
	M.connects[name].connection:connect(M.connects[name].host, M.connects[name].port)

	print('SOCKET: Class reconnect')
	M.checkConnection(name, M.tryReconnectHandler)
end

--[====[
	@type func
	@name M.sendMessage(name, message)
	@param name:string Имя сокет канала
	@param message:string Сообщение для отправки
	@brief Отправить сообщение на сервер
--]====]
M.sendMessage = function(name, message)
	if M.connects[name].state == M.EventTypes.CONNECT then
		if M.connects[name].sendMessageTime == nil then
			M.connects[name].sendMessageTime = os.time()
		end
		
		M.connects[name].connection:send(message)
	end
end

--[=====[
    @type func
    @name libSocket.checkConnects
    @param name:string Имя клиент-сокета, если не указано, то будут проверены все соединения
    @brief Возвращает сокет-клиенты, которым не удалось подключится к серверу
--]=====]
M.checkConnects = function(name)
	local connects

	if name ~= nil then
		if not M.connects[name] then
			return false
		end

		connects = {}
		connects[name] = M.connects[name]
	else
		connects = M.connects
	end

	local notConnected = {}

	for socketClientName, socketClient in pairs(M.connects) do
		if socketClient.state ~= M.EventTypes.CONNECT then
			notConnected[socketClientName] = socketClient
		end
	end

	return notConnected
end

--[=====[
    @type func
    @name libSocket.callEventListeners
    @param name:string Имя клиент-сокета
    @param eventType:string Тип события из libSocket.EventTypes
    @param event:any Обьект события
    @brief Вызывает все слушатели указанного события. Можно передать объект события - любой ти
--]=====]
M.callEventListeners = function(name, eventType, event)
	if M.connects[name].events[eventType] == nil then
		return
	end

	for k, handlers in pairs(M.connects[name].events[eventType]) do
		handlers(event)
	end
end

--[=====[
	@type func
	@name libSocket.connect
	@param host:string Имя хоста
	@param port:number Порт хоста
	@brief Созадет tcp соединение с сервером
--]=====]
M.connect = function(host, port)
	local connection = assert(socket.tcp(), 'Cant get socket.tcp')
	connection:settimeout(0)
	connection:connect(host, port)

	return connection
end

--[====[
	@type func
	@name M.closeConnection(name)
	@param name:string Имя сокет канала
	@brief Закрыть сокет соединение
--]====]
M.closeConnection = function(name)
	M.connects[name].connection:close()
end

--[=====[
	@type func
	@name libSocket.checkConnectionToServer
	@param host:string Имя хоста
	@param port:number Порт хоста
	@param handler:func Функция обратного вызова
	@brief Пытается подсоединится у казанному хосту, в случае успеха/таймауто вызовет колбек
--]=====]
M.checkConnectionToServer = function(host, port, handler)
	local period = 20
	local timeout = 1000
	local currTime = 0
	local timeoutTimer

	local connection = assert(socket.tcp(), 'Cant get socket.tcp')
	connection:settimeout(0)
	connection:connect(host, port)

	timeoutTimer = timer.performWithDelay(period, function()
			currTime = currTime + period

			local input, writeReady, err = socket.select(nil, {connection}, 0)

			if writeReady[1] then
				timer.cancel(timeoutTimer)

				local data = {
					result = M.EventTypes.CONNECT,
					connection = connection
				}

				handler(data)
			end

			if currTime >= timeout then
				print('SOCKET: timeout')
				connection:close()
				timer.cancel(timeoutTimer)

				local data = {
					result = M.EventTypes.CANT_CONNECT_TO_SERVER
				}

				handler(data)
			end
		end, 0)
end

return M