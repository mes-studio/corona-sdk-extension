---------------------------------
--
-- v_0.0.3
-- libLanguage.lua - Локализация
--
---------------------------------

local M = {
	--	текущий язык
	lng = nil
}

--[====[
	@type func
	@name M.init()
	@brief Первичная инициализация
--]====]
M.init = function ()
	M.detectLng()
end

--[====[
	@type func
	@name M.detectLng()
	@brief Определить/Установить язык
--]====]
M.detectLng = function (language)
	local lng

	if language then
		lng = language
	else
		lng = system.getPreference("locale", "language")
	end

	M.lng = string.lower(lng)

	EVENT.trigger('change.language', M.lng)
end

--[====[
	@type func
	@name M.language()
	@brief Получить/Установить
--]====]
M.language = function (language)
	if empty(language) then
		return M.lng
	else
		M.detectLng(language)
	end
end

return M
