-----------------------------
--
-- v_0.0.3
-- libEvent.lua - библиотека событий. Как единый стек ивентов
--
-----------------------------

local M = {
	-- стек событий
	stackEvent = {}
}

--[====[
	@type func
	@name M.on(eventName, handler)
	@param eventName:string Имя события
	@param handler:function Листенер события
	@brief Добавляет листенер к событию
--]====]
M.on = function (eventName, handler)
	table.insert(M.stackEvent, {
		name = eventName,
		handler = handler
	})
end

--[====[
	@type func
	@name M.trigger(eventName)
	@param eventName:string Имя события
	@brief Вызывает событие
--]====]
M.trigger = function (eventName, eventData)
	callEvent(M.stackEvent, eventName, eventData)
end

--[====[
	@type func
	@name M.off(eventName)
	@param eventName:string Имя события
	@brief Удаляет все обработчик по указанному событию
--]====]
M.off = function (eventName)
	removeEvent(M.stackEvent, eventName, true)
end

--[====[
	@type func
	@name M.unbind(eventName, handler)
	@param eventName:string Имя события
	@param handler:function Листенер события
	@brief Удаляет обработчик по указанному событию
--]====]
M.unbind = function (eventName, handler)
	removeEvent(M.stackEvent, eventName, handler)
end

return M
