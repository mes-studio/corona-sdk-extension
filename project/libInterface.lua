---------------------------------------------------------------------------------
--
-- v_0.0.3
-- Function for work with UI
--
---------------------------------------------------------------------------------

-- Screen parameters
_CX = display.contentCenterX
_CY = display.contentCenterY
_W = display.contentWidth
_H = display.contentHeight
_HD_H = 1280
_HD_W = 720

local M = {
	--	список всех создаваемых текстовых полей
	textList = {}
}
local widget = require "widget"

--[====[
	@type func
	@name init()
	@brief Первичная инициализация библиотеки
--]====]
M.init = function ()
	-- Сменить текст во всех текстовых полях при смене языка
	EVENT.on('change.language', function (lng)
		for _, item in pairs(M.textList) do
			M.findText(item)
		end
	end)
end

--[====[
	@type func
	@name findText(obj)
	@param obj:table текстовый объект
	@brief Из `obj.textPath` нахоидт нужный текст текущего языка и приминяет его
--]====]
M.findText = function (obj)
	if empty(obj) then
		return
	end

	local path = obj.textPath
	local pathList = string.split(path, ".")
	local text = TEXT[LANG.language()]

	for _, item in pairs(pathList) do
		if item ~= 'TEXT' then
			text = text[item]
		end
	end

	obj.text = text
end

-- СОЗДАНИЕ --
--[====[
	@type func
	@name GUI.createGroup()
	@return group группа
	@brief Создание группы
--]====]
M.createGroup = function()
	return display.newGroup()
end

--[====[
	@type func
	@name GUI.createRect{params}
	@param group:group группа
	@param x:number положение по оси Х
	@param y:number положение по оси Y
	@param alpha:number прозрачность
	@param color:table цвет текста в формате R/255,G/255,B/255
	@param width:number ширина
	@param height:number высота
	@brief Создание квадрата
--]====]
M.createRect = function(params)
	local rect = display.newRect( params.group, params.x, params.y, params.width, params.height )

	if params.alpha then
		rect.alpha = params.alpha
	end

	if params.color then
		rect:setFillColor( params.color )
	end

	if params.event then
		rect:addEventListener('touch', params.event)
	end

	return rect
end

--[====[
	@type func
	@name GUI.createRoundRect{params}
	@param group:group группа
	@param x:number положение по оси Х
	@param y:number положение по оси Y
	@param alpha:number прозрачность
	@param color:table цвет текста в формате R/255,G/255,B/255
	@param width:number ширина
	@param height:number высота
	@param radius:number радиус
	@brief Создание квадрата с округленными углами
--]====]
M.createRoundRect = function(params)
	local rect = display.newRoundedRect( params.group, params.x, params.y, params.width, params.height, params.radius )

	if params.alpha then
		rect.alpha = params.alpha
	end

	if params.color then
		rect:setFillColor( params.color )
	end

	return rect
end

--[====[
	@type func
	@name GUI.createRect{params}
	@param group:group группа
	@param x:number положение по оси Х
	@param y:number положение по оси Y
	@param radius:number радиус
	@param alpha:number прозрачность
	@brief Создание круга
--]====]
M.createCircle = function(params)
	local circle = display.newCircle( params.group, params.x, params.y, params.radius )

	if params.alpha then
		circle.alpha = params.alpha
	end

	return circle
end

--[====[
	@type func
	@name GUI.createSheet{params}
	@param img:string изображение
	@param width:number ширина кадра
	@param height:number высота кадра
	@param countFrame:number количество кадров
	@brief Создание настроек изображения
--]====]
M.createSheet = function(params)
	return graphics.newImageSheet(
			params.img, {
			width = params.width,
			height = params.height,
			numFrames = params.numFrames
		} )
end

--[====[
	@type func
	@name GUI.createSprite{params}
	@param group:group группа
	@param sheet:object картинка с параметрами
	@param start:number  стартовый кадр
	@param stop:number  конечный кадр
	@param time:number время воспроизведения
	@param loop:number  количество повторений
	@brief Создание спрайта
--]====]
M.createSprite = function(params)
	local sequenceData =
	{
		start = params.start,
		count = params.count,
		time = params.time,
		loopCount = params.loopCount,
	}

	return display.newSprite( params.group, params.sheet, sequenceData )
end

--[====[
	@type func
	@name GUI.createText{params}
	@param group:group группа
	@param text:string текст
	@param align:string привязка текста
	@param x:number положение по оси Х
	@param y:number положение по оси Y
	@param width:number ширина текста
	@param height:number высота текста
	@param color:table цвет текста в формате R/255,G/255,B/255
	@param font:number шрифт
	@param fontSize:number размер шрифта
	@param alpha:number прозрачность
	@brief Создание текста
--]====]
M.createText = function(params)
	local path = 'assets/font/future'
	local font = params.font and params.font or path

	local options = {
		parent = params.group,
		--text = params.text,
		text = '',
		x = params.x,
		y = params.y,
		width = params.width,
		height = params.height,
		font = font,
		fontSize = params.size,
		align = params.align
	}

	local text = display.newText( options )
	text:setFillColor( params.color )

	if params.alpha then
		text.alpha = params.alpha
	end

	-- путь к тексту
	text.textPath = params.text
	-- добавить в список
	M.textList[#M.textList + 1] = text

	--	назначить текст
	M.findText(text)

	return text
end

--[====[
	@type func
	@name GUI.createInputText{params}
	@param group:group группа
	@param x:number положение по оси Х
	@param y:number положение по оси Y
	@param width:number ширина текста
	@param height:number высота текста
	@param hasBackground:number флаг фона
	@param font:number шрифт
	@param fontSize:number размер шрифта
	@param text:string текст
	@param align:string привязка текста
	@param alpha:number прозрачность
	@param inputType:number слушатель на событие пользовательского ввода
	@return table Объект поля ввода
	@brief Создание вводного текса
--]====]
M.createInputText = function (params)
	local inputText = native.newTextField(params.x, params.y, params.width, params.height)

	if not empty(params.group) then
		params.group:insert(inputText)
	end

	if not empty(params.hasBackground) then
		inputText.hasBackground = params.hasBackground
	end

	if not empty(params.font) then
		inputText.font = params.font
	end

	if not empty(params.fontSize) then
		inputText.size = params.fontSize
	end

	if not empty(params.text) then
		inputText.text = params.text
	end

	if not empty(params.align) then
		inputText.align = params.align
	end

	if not empty(params.alpha) then
		inputText.alpha = params.alpha
	end

	if not empty(params.inputType) then
		inputText.inputType = params.inputType
	end

	if not empty(params.userInput) then
		inputText:addEventListener("userInput", params.userInput)
	end

	return inputText
end

--[====[
	@type func
	@name GUI.createImage{params}
	@param group:group группа
	@param img:string картинка
	@param x:number положение по оси Х
	@param y:number положение по оси Y
	@param xScale:number размер по X
	@param yScale:number размер по Y
	@param width:number ширина
	@param height:number высота
	@param alpha:number прозрачность
	@brief Создание изображения
--]====]
M.createImage = function(params)
	local img = display.newImage( params.img, params.x, params.y, true )

	if params.xScale then
		img.xScale = params.xScale
	end

	if params.yScale then
		img.yScale = params.yScale
	end

	if params.width then
		img.width = params.width
	end

	if params.height then
		img.height = params.height
	end

	if params.alpha then
		img.alpha = params.alpha
	end

	if params.group then
		params.group:insert( img )
	end

	if params.event then
		img:addEventListener( 'touch', params.event )
	end

	return img
end

--[====[
	@type func
	@name GUI.createTimer{params}
	@param time:number время тика
	@param loop:number количество повторений
	@param event:func событие
	@brief Создание таймера
--]====]
M.createTimer = function(params)
	return timer.performWithDelay( params.time, params.event, params.loop )
end

--[====[
	@type func
	@name GUI.createButton{params}
	@param group:group группа
	@param x:number положение по Х
	@param y:number положение по Y
	@param xScale:number размер по Х
	@param yScale:number размер по Y
	@param rotation:number поворот
	@param size:number размер шрифта
	@param alpha:number прозрачность
	@param theme:string цветовая схема кнопки( 'orange', 'purple' )
	@param font:string шрифт
	@param align:string привязка текста
	@param id:string ид кнопки
	@param enabled:boolean работоспособность
	@param def_color:table цвет текста в формате R/255,G/255,B/255
	@param over_color:table цвет текста при нажатии в формате R/255,G/255,B/255
	@param event:func событие
	@brief Создание кнопки
--]====]
M.createButton = function(params)
	local options = {
		width = 612,
		height = 157,
		numFrames = 2,
		sheetContentWidth = 618,
		sheetContentHeight = 320
	}

	local path = 'assets/images/ui/'
	local theme = params.theme and params.theme or 'orange'
	local sheet = graphics.newImageSheet( path..theme..'.png', options )
	path = 'assets/font/astakhov'
	local font = params.font and params.font or path

	local button = widget.newButton(
		{
			id = params.id,
			sheet = sheet,
			defaultFrame = 1,
			overFrame = 2,
			label = params.text,
			isEnabled = params.enabled,
			labelAlign = params.align,
			labelColor = {
					default = params.def_color,
					over = params.over_color
				},
			font = font,
			fontSize = params.size,
			onEvent = params.event
		}
	)

	button.x = params.x
	button.y = params.y

	if params.xScale then
		button.xScale = params.xScale
	end

	if params.yScale then
		button.yScale = params.yScale
	end

	if params.rotation then
		button.rotation = params.rotation
	end

	if params.alpha then
		button.alpha = params.alpha
	end

	if params.group then
		params.group:insert( button )
	end

	return button
end

--[====[
	@type func
	@name GUI.createTextBox{params}
	@param group:group группа
	@param x:number положение по Х
	@param y:number положение по Y
	@param width:number ширина рабочей области
	@param height:number высота рабочей области
	@param size:number размер шрифта
	@param alpha:number прозрачность
	@param font:string шрифт
	@param align:string привязка текста
	@param color_rect:table цвет квадрата в формате R/255,G/255,B/255
	@param color_text:table цвет текста в формате R/255,G/255,B/255
	@brief Создание текстового бокса
--]====]
M.createTextBox = function(params)
	local x = params.x and params.x or _W/2
	local y = params.y and params.y or _H/2
	local width = params.width and params.width or _H*.5
	local height = params.height and params.height or _H*.15
	local text = params.text and params.text or 'Test'
	local color_rect = params.color_rect and params.color_rect or 255
	local color_text = params.color_text and params.color_text or 0

	local gr = display.newGroup()
	local rect = display.newRect( gr, x, y, width, height )
	rect.alpha = .6

	local path = 'assets/font/future'
	local font = params.font and params.font or path

	local options =
	{
		parent = gr,
		text = text,
		x = rect.x,
		y = rect.y,
		width = rect.width-_H*.04,
		font = font,
		fontSize = params.size,
		align = params.align
	}
	local text = display.newText( options )

	if params.alpha then
		gr.alpha = params.alpha
	end

	if params.group then
		params.group:insert( gr )
	end

	rect:setFillColor( color_rect[1], color_rect[2], color_rect[3] )
	text:setFillColor( color_text[1], color_text[2], color_text[3] )

	return gr
end

--[====[
	@type func
	@name GUI.createScroll{params}
	@param width:number ширина рабочей области
	@param height:number высота рабочей области
	@param scrollWidth:number ширина прокрутки
	@param scrollHeight:number высота прокрутки
	@param scrollHeight:number высота прокрутки
	@param friction:number трение
	@param locked:boolean заблокирована ли прокрутка
	@param bounce:boolean эффект отскакивания от краев
	@param horizontal:boolean блокировка горизонтального скроллинга
	@param vertical:boolean блокировка вертикального скроллинга
	@param hideBackground:boolean показать фон
	@param hideScrollBar:boolean показать скролл бар
	@param event:func событие
	@brief Создание скролла
--]====]
M.createScroll = function(params)
	local scrollView = widget.newScrollView( {
		x = params.x,
		y = params.y,
		width = params.width,
		height = params.height,
		scrollWidth = params.scrollWidth,
		scrollHeight = params.scrollHeight,
		isLocked = params.locked,
		friction = params.friction,
		isBounceEnabled = params.bounce,
		horizontalScrollDisabled = params.horizontal,
		verticalScrollDisabled = params.vertical,
		hideBackground = params.background,
		hideScrollBar = params.bar,
		listener = params.event
	} )

	return scrollView
end

-- ФУНКЦИИ
--[====[
	@type func
	@name GUI.setColor(obj,r,g,b)
	@param obj:object объект
	@param r:number цвет по R
	@param b:number цвет по G
	@param g:number цвет по B
	@brief Установить цвет
--]====]
M.setColor = function(obj,r,g,b)
	obj:setFillColor( r/255, g/255, b/255 )
end

--[====[
	@type func
	@name GUI.remove(obj)
	@param obj:object объект
	@brief Удалить
--]====]
M.remove = function(obj)
	display.remove(obj)
	obj = nil
end

--[====[
	@type func
	@name GUI.buttonEnabled(obj,state)
	@param obj:object объект
	@param state:boolean состояние
	@brief Установить работоспособность кнопки
--]====]
M.buttonEnabled = function(obj,state)
	obj:setEnabled( state )
end

--[====[
	@type func
	@name GUI.insert(object1,object2)
	@param object1:object главный объект
	@param object2:object подданый
	@brief Соединение объектов
--]====]
M.insert = function(object1,object2)
	object1:insert( object2 )
end

--ЭФФЕКТЫ
--[====[
	@type func
	@name GUI.transition{params}
	@param obj:object объект
	@param time:number время
	@param delay:number задержка
	@param x:number положение по оси Х
	@param y:number положение по оси Y
	@param width:number ширина
	@param height:number высота
	@param xScale:number размер по X
	@param yScale:number размер по Y
	@param rotation:number поворот
	@param alpha:number прозрачность
	@param tag:string имя анимации(для паузы и воспроизведения)
	@param start:func выполняется при старте
	@param complete:func выполняется по завершению
	@param pause:func выполняется при паузе
	@param resume:func выполнится при повторе
	@param cancel:func выполнится при прерывании
	@param loop:func выполнится при повторении
	@brief Настраиваемое перемещение
--]====]
M.transition = function(params)
	return transition.to( params.obj, {
				time = params.time,
				x = params.x,
				y = params.y,
				rotation = params.rotation,
				alpha = params.alpha,
				xScale = params.xScale,
				yScale = params.yScale,
				delay = params.delay,
				width = params.width,
				height = params.height,
				tag = params.tag,
				onStart = params.start,
				onComplete = params.complete,
				onPause = params.pause,
				onResume = params.resume,
				onCancel = params.cancel,
				onRepeat = params.loop,
			} )
end

--[====[
	@type func
	@name GUI.fadeOut{params}
	@param obj1:object объект 1
	@param obj2:object объект 2
	@param time:number время
	@param delay:number задержка
	@brief Переход с одного бъекта на другой
--]====]
M.dissolve = function(params)
	return transition.dissolve( params.obj1, params.obj2, params.time, params.delay )
end

--[====[
	@type func
	@name GUI.blink{params}
	@param obj:object объект
	@param time:number время
	@param delay:number задержка
	@param start:func выполняется при старте
	@param pause:func выполняется при паузе
	@param resume:func выполнится при повторе
	@param cancel:func выполнится при прерывании
	@param loop:func выполнится при повторении
	@brief Мигание
--]====]
M.blink = function(params)
	return transition.blink( params.obj, {
				time = params.time,
				delay = params.delay,
				onStart = params.start,
				onPause = params.pause,
				onResume = params.resume,
				onCancel = params.cancel,
				onRepeat = params.loop,
			} )
end

--[====[
	@type func
	@name GUI.fadeIn{params}
	@param obj:object объект
	@param time:number время
	@param delay:number задержка
	@param start:func выполняется при старте
	@param complete:func выполняется по завершению
	@param pause:func выполняется при паузе
	@param resume:func выполнится при повторе
	@brief Появление
--]====]
M.fadeIn = function(params)
	return transition.fadeIn( params.obj, {
				time = params.time,
				delay = params.delay,
				onStart = params.start,
				onComplete = params.complete,
				onPause = params.pause,
				onResume = params.resume,
				onCancel = params.cancel,
			} )
end

--[====[
	@type func
	@name GUI.fadeOut{params}
	@param obj:object объект
	@param time:number время
	@param delay:number задержка
	@param start:func выполняется при старте
	@param complete:func выполняется по завершению
	@param pause:func выполняется при паузе
	@param resume:func выполнится при повторе
	@brief Исчезновение
--]====]
M.fadeOut = function(params)
	return transition.fadeOut( params.obj, {
				time = params.time,
				delay = params.delay,
				onStart = params.start,
				onComplete = params.complete,
				onPause = params.pause,
				onResume = params.resume,
				onCancel = params.cancel,
			} )
end

return M