local scene = composer.newScene()
local M = {
	group = nil
}

function scene:show( event )
	if "will" == event.phase then
		M.group = self.view

		local rect = GUI.createRect {
			group = M.group,
			x = _CX,
			y = _CY,
			width = _W,
			height = _H,
			event = function (e)
				if e.phase == 'ended' then
					print('click')
					UTILS.gotoScene('sc2', 'fade', 500)
				end
			end
		}

		local text = GUI.createText {
			group = M.group,
			x = _CX,
			y = _CY,
			text = "TEXT.player.name",
			size = _H * .06,
		}
		text:setTextColor(0, 0, 0)

		timer.performWithDelay(1500, function ()
			LANG.language('en')
		end, 1)
	end
end

function scene:hide( event )
	if "will" == event.phase then
		print( 'hide' )
	end
end

---------------------------------------------------------------------------------

scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )

---------------------------------------------------------------------------------

return scene

