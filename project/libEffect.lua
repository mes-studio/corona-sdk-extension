----------------------------
--
-- v_0.0.3
-- svcEffect.lua - библиотека эффектов
--
----------------------------

local M = {}

--[====[
	@type func
	@name M.shake()
	@param params:table Входные параметры
	@param params.obj:table Объект отображение
	@parma params.delay:number Задержка
	@parma params.time:number Время одной тряски
	@parma params.radius:number Радиус тряски
	@parma params.numShake:number Число трясок
	@parma params.horizontal:bool Горизонтально или Вертикально будет трясти
	@parma params.fromSide:bool В какую сторону трясти
	@parma params.count:number Текущая тряска
	@parma params.callback:function Колбек. Вызывается после окончания анимации
	@brief "Трясет" объект в стороны
--]====]
M.shake = function(params)
	local obj = params.obj
	local numShake = params.numShake
	local count = params.count

	if empty(count) then
		count = 0
	end

	local fromSide = params.fromSide
	local time = params.time
	local delay = params.delay
	local callback = params.callback
	local radius = params.radius
	local horizontal = params.horizontal

	-- рабочие переменные
	local x = obj.x
	local y = obj.y
	local workTime = time

	if count ~= 0 then
		delay = 0
	end

	if horizontal then
		x = radius

		if fromSide then
			x = -x
		end

		if count == 0 or count == numShake then
			x = x / 2
			workTime = workTime / 2
		end

		x = obj.x + x
	else
		y = radius

		if fromSide then
			y = -y
		end

		if count == 0 or count == numShake then
			y = y / 2
			workTime = workTime / 2
		end

		y = obj.y + y
	end

	if count <= numShake then
		obj.transition = GUI.transition {
			obj = obj,
			delay = delay,
			time = workTime,
			x = x,
			y = y,
			complete = function ()
				M.shake {
					obj = obj,
					time = time,
					horizontal = horizontal,
					numShake = numShake,
					count = count + 1,
					radius = radius,
					fromSide = not fromSide,
					callback = callback
				}
			end
		}
	else
		if not empty(callback) then
			callback()
		end
	end
end

--[====[
	@type func
	@name M.zoom()
	@param params:table Входные параметры
	@param params.obj:table Объект отображение
	@param params.delay:number Задержка
	@parma params.time:number Время зума
	@param params.zoom:number Значение зума
	@parma params.callback:function Колбек. Вызывается после окончания анимации
	@brief "Зумирует" объект
--]====]
M.zoom = function (params)
	local obj = params.obj
	local delay = params.delay
	local time = params.time
	local zoom = params.zoom
	local callback = params.callback

	GUI.transition {
		obj = obj,
		delay = delay,
		time = time,
		xScale = zoom,
		yScale = zoom,
		complete = function ()
			if not empty(callback()) then
				callback()
			end
		end
	}
end

--[====[
	@type func
	@name M.zoomInOut()
	@param params:table Входные параметры
	@param params.obj:table Объект отображение
	@param params.delay:number Задержка
	@parma params.timeIn:number Время зума на увеличение
	@parma params.timeOut:number Время зума на уменьшение
	@param params.zoomIn:number Значение зума на увеличение
	@param params.zoomOut:number Значение зума на уменьшение
	@parma params.callback:function Колбек. Вызывается после окончания анимации
	@brief "Зумирует" объект
--]====]
M.zoomInOut = function (params)
	local obj = params.obj
	local delay = params.delay
	local timeIn = params.timeIn
	local timeOut = params.timeOut
	local zoomIn = params.zoomIn
	local zoomOut = params.zoomOut
	local callback = params.callback

	M.zoom {
		obj = obj,
		delay = delay,
		time = timeIn,
		zoom = zoomIn,
		callback = function ()
			M.zoom {
				obj = obj,
				time = timeOut,
				zoom = zoomOut,
				callback = callback
			}
		end
	}
end

--[====[
	@type func
	@name M.hide()
	@param params:table Входные параметры
	@param params.obj:table Объект отображение
	@param params.delay:number Задержка
	@parma params.time:number Время скрытия
	@parma params.callback:function Колбек. Вызывается после окончания анимации
	@brief "Скрывает" объект
--]====]
M.hide = function (params)
	local obj = params.obj
	local delay = params.delay
	local time = params.time
	local callback = params.callback

	GUI.transition {
		obj = obj,
		delay = delay,
		time = time,
		alpha = 0,
		complete = function ()
			if not empty(callback) then
				callback()
			end
		end
	}
end

--[====[
	@type func
	@name M.show()
	@param params:table Входные параметры
	@param params.obj:table Объект отображение
	@param params.delay:number Задержка
	@parma params.time:number Время скрытия
	@parma params.callback:function Колбек. Вызывается после окончания анимации
	@brief "Показывает" объект
--]====]
M.show = function (params)
	local obj = params.obj
	local delay = params.delay
	local time = params.time
	local callback = params.callback

	GUI.transition {
		obj = obj,
		delay = delay,
		time = time,
		alpha = 1,
		complete = function ()
			if not empty(callback) then
				callback()
			end
		end
	}
end

return M
