-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

composer = require "composer"

-- расширения
require "libString"

GUI = require "libInterface"
UTILS = require "libUtils"
EVENT = require "libEvent"
LANG = require "libLanguage"

TEXT = require "dbLanguage"

-- Initialization start
GUI.init()
-- инициализацию LANG нужно ставить в конец!!!
LANG.init()
-- Initialization end

UTILS.gotoScene('sc1', 'fade', 500)
