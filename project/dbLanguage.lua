---------------------------------
--
-- v_0.0.3
-- dbLanguage.lua - Локализация. Содержится только текст
--
---------------------------------

local text = {
	ru = {
		player = {
			name = 'Стас',
			surname = 'Пишевский'
		}
	},
	en = {
		player = {
			name = 'Stas',
			surname = 'Pishevskyi'
		}
	}
}

return text
