---------------------------------------------------------------------------------
--
-- v_0.0.3
-- Шаблоны функций
--
---------------------------------------------------------------------------------

local json = require( "json" )

local M = {}

--[====[
	@type func
	@name callEvent(stack, eventName, eventData)
	@param stack:table Стек ивентов
	@param eventName:string Название события
	@param eventData:any Данные события
	@brief Вызывает событие по имени и передает данные события
--]====]
function callEvent(stack, eventName, eventData)
	for key, item in pairs(stack) do
		if stack[key].name == eventName then
			if not empty(stack[key].handler) then
				stack[key].handler(eventData)
			end
		end
	end
end

--[====[
	@type func
	@name removeEvent(stack, eventName, func)
	@param stack:table Стек ивентов
	@param eventName:string Название события
	@param func:function Листенер. Если <b>true</b>, то удалит все листенеры
	@brief Удаляет обработчик указанного события со стека иветов
--]====]
function removeEvent (stack, eventName, func)
	for key, item in pairs(stack) do
		if isFunction(func) then
			if eventName == stack[key].name and func == stack[key].handler then
				table.remove(stack, key)

				break
			end
		elseif isBool(func) then
			if eventName == stack[key].name then
				table.remove(stack, key)

				break
			end
		end
	end
end

-- СОЗДАНИЕ --
--[====[
	@type func
	@name FUNC.createTimer(params)
	@param time:number время тика
	@param i:number итератор таймера
	@param event:func функция на тик
	@brief Создать таймер
--]====]
M.createTimer = function(params)
	return timer.performWithDelay( params.time, params.event, params.i )
end

-- ФУНКЦИИ
--[====[
	@type func
	@name FUNC.timerPause(obj)
	@param obj:object объект
	@brief Поставить таймер на паузу
--]====]
M.timerPause = function(obj)
	timer.pause( obj )
end

--[====[
	@type func
	@name FUNC.timerResume(obj)
	@param obj:object объект
	@brief Воспроизвести таймер
--]====]
M.timerResume = function(obj)
	timer.resume( obj )
end

--[====[
	@type func
	@name FUNC.timerStop(obj)
	@param obj:object объект
	@brief Остановить таймер
--]====]
M.timerStop = function(obj)
	timer.cancel( obj )
end

M.gotoScene_ = {
	group = GUI.createGroup(),
	firstScene = true
}
M.gotoScene_.bg = GUI.createRect {
	group = M.gotoScene_.group,
	x = _CX,
	y = _CY,
	width = _W,
	height = _H,
	alpha = 0
}
M.gotoScene_.bg.isVisible = false
M.gotoScene_.bg:addEventListener('touch', function () return true end)
M.gotoScene_.bg:addEventListener('tap', function () return true end)

--[====[
	@type func
	@name FUNC.gotoScene(obj)
	@param scene:string имя сцены
	@param anim:string имя анимации перехода
	@param time:number время анимации перехода
	@brief переход на сцену
--]====]
M.gotoScene = function(scene, anim, time)
	EVENT.trigger('before.gotoScene')

	local options = {
			effect = anim,
			time = time,
		}

	M.gotoScene_.bg.isVisible = true
	M.gotoScene_.bg:setFillColor(0)
	M.gotoScene_.group:toFront()
	M.alpha = 0

	-- Удалить сцену
	local currScene = composer.getSceneName("current")

	local time2 = time / 6
	local time1 = (time - time2) / 2

	if time ~= nil and anim ~= nil then
		--	Если это загрузка первой сцена, начинаем анимацию со второй части
		if anim == 'fade' then
			if M.gotoScene_.firstScene then
				M.gotoScene_.firstScene = false
				M.gotoScene_.bg.alpha = 1
				composer.gotoScene(scene)

				GUI.transition {
					obj = M.gotoScene_.bg,
					delay = time / 4,
					alpha = 0,
					complete = function ()
						M.gotoScene_.bg.isVisible = false

						EVENT.trigger('after.gotoScene')
					end
				}
			else
				GUI.transition {
					obj = M.gotoScene_.bg,
					time = time1,
					alpha = 1,
					complete = function ()
						composer.gotoScene(scene)

						GUI.transition {
							obj = M.gotoScene_.bg,
							time = time2,
							alpha = 0,
							complete = function ()
								M.gotoScene_.bg.isVisible = false

								EVENT.trigger('after.gotoScene')
								M.removeScene(currScene)
							end
						}
					end
				}
			end
		end
	else-- обычное перемещение по сцене
		composer.gotoScene( scene )
		EVENT.trigger('after.gotoScene')
	end

	--composer.gotoScene( scene, options )
end

--[====[
	@type func
	@name FUNC.removeScene(obj)
	@param scene:string имя сцены
	@brief удаление сцены
--]====]
M.removeScene = function(scene)
	composer.removeScene(scene)
end


-- ГЛОБАЛЬНЫЕ --
--[====[
	@type func
	@name name print_r(t)
	@param t:table Table that need pring
	@brief Displays the table structure
--]====]
function print_r(t)
	local print_r_cache={}

	local function sub_print_r(t,indent)
		if (print_r_cache[tostring(t)]) then
			print(indent.."*"..tostring(t))
		else
			print_r_cache[tostring(t)]=true

			if (type(t)=="table") then
				for pos,val in pairs(t) do
					if (type(val)=="table") then
						print(indent.."["..pos.."] => "..tostring(t).." {")
						sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
						print(indent..string.rep(" ",string.len(pos)+6).."}")
					elseif (type(val)=="string") then
						print(indent.."["..pos..'] => "'..val..'"')
					else
						print(indent.."["..pos.."] => "..tostring(val))
					end
				end
			else
				print(indent..tostring(t))
			end
		end
	end

	if (type(t)=="table") then
		print(tostring(t).." {")
		sub_print_r(t,"  ")
		print("}")
	else
		sub_print_r(t,"  ")
	end

	print()
end

function serverReport (data)
	if isTable(data) then
		data = json.encode(data)
	end

	HTTP.request{
		url = 'http://194.242.102.33:3000/report',
		method = 'POST',
		body = {
			report = data
		}
	}
end

--[====[
	@type func
	@name name isset(obj, key)
	@param obj:table Input table
	@param key:any Input table key
	@return bool <b>TRUE</b> if value is set
	@brief Check if this value is set by key
--]====]
function isset(obj, key)
	if type(obj) ~= "table" then
		return false
	end

	return not empty(obj[key])
end

--[====[
	@type func
	@name name empty(obj)
	@param obj:any Input value
	@return bool <b>TRUE</b> if value is empty
	@brief Check if this value is empty
--]====]
function empty(obj)
	if obj == nil or obj == "" or obj == false or obj == 0 or obj == '0' or (isTable(obj) and count(obj) == 0) then
		return true
	end

	return false
end

--[====[
	@type func
	@name count(obj)
	@param obj:table Input table
	@return number/nil Number of elements
	@brief Calculate the exact number of elements in array. Return <b>nil</b> if input <i>obj</i> is not table
--]====]
function count(obj)
	if type(obj) ~= "table" then
		return nil
	end

	local count = 0

	for k, v in pairs(obj) do
	   count = count + 1
	end

	return count
end

--[====[
	@type func
	@name getItem(obj, key, default?)
	@param obj:table Input table
	@param key:string Table key
	@param default:any Optional. Default value
	@return any Searched value
	@brief Return value by key, if value by key don't exist, than return <b>default</b> value, if <b>default</b> value don't set, than return <b>nil</b>
--]====]
function getItem(obj, key, default)
	if type(obj) ~= "table" then
		return nil
	end

	if empty(obj[key]) then
		if empty(default) then
			return nil
		end

		return default
	end

	return obj[key]
end

--[====[
	@type func
	@name inArray(table, needed)
	@param table:table Table
	@param needed:any Needed value
	@return any <b>nil</b> if value not exit
	@brief Try find key by value in table
--]====]
function inArray(table, needed)
	local result = nil

	for key, item in pairs(table) do
		if item == needed then
			result = key

			break
		end
	end

	return result
end

--[====[
	@type func
	@name isString(obj)
	@param obj:any Any input value
	@return bool <b>TRUE</b> if string, <b>FALSE</b> in another way
	@brief Check value type
--]====]
function isString(obj)
	return type(obj) == "string"
end

--[====[
	@type func
	@name isNil(obj)
	@param obj:any Any input value
	@return bool <b>TRUE</b> if nil, <b>FALSE</b> in another way
	@brief Check value type
--]====]
function isNil(obj)
	return type(obj) == "nil"
end

--[====[
	@type func
	@name isNil(obj)
	@param obj:any Any input value
	@return bool <b>TRUE</b> if table, <b>FALSE</b> in another way
	@brief Check value type
--]====]
function isTable(obj)
	return type(obj) == "table"
end

--[====[
	@type func
	@name isNumber(obj)
	@param obj:any Any input value
	@return bool <b>TRUE</b> if number, <b>FALSE</b> in another way
	@brief Check value type
--]====]
function isNumber(obj)
	return type(obj) == "number"
end

--[====[
	@type func
	@name isFunction(obj)
	@param obj:any Any input value
	@return bool <b>TRUE</b> if function, <b>FALSE</b> in another way
	@brief Check value type
--]====]
function isFunction(obj)
	return type(obj) == "function"
end
--[====[
	@type func
	@name isBool(obj)
	@param obj:any Any input value
	@return bool <b>TRUE</b> if boolean, <b>FALSE</b> in another way
	@brief Check value type
--]====]
function isBool(obj)
	return type(obj) == "boolean"
end

--[====[
	@type func
	@name getVar(obj, default)
	@param obj:any Any input value
	@param default:any Any default value
	@return any
	@brief Return default value if <b>obj</b> is empty, otherwise return <b>obj</b> value
--]====]
function getVar(obj, default)
	if empty(obj) then
		return default
	else
		return obj
	end
end

--[====[
	@type func
	@name copyTable(obj)
	@param obj:table Входящая таблица
	@return table Скопированная таблица
	@brief Делает полное копирование таблицы
--]====]
function copyTable(obj)
	local result = {}

	for key, item in pairs(obj) do
		if isTable(item) then
			result[key] = copyTable(item)
		else
			result[key] = item
		end
	end

	return result
end

--[====[
	@type func
	@name rgbToCmyk(r, g, b)
	@param r:number R цвет
	@param g:number G цвет
	@param b:number B цвет
	@return CMYK
	@brief Переводит RGB в CMYK
--]====]
function rgbToCmyk (r, g, b)
	local r = r / 255
	local g = g / 255
	local b = b / 255

	return r, g, b
end

--[====[
	@type func
	@name hexToCmyk(r, g, b)
	@param r:number R цвет
	@param g:number G цвет
	@param b:number B цвет
	@return CMYK
	@brief Переводит HEX в CMYK
--]====]
function hexToCmyk (hex)
	local r = tonumber(string.sub(hex, 1, 2), 16)
	local g = tonumber(string.sub(hex, 3, 4), 16)
	local b = tonumber(string.sub(hex, 5, 6), 16)

	return rgbToCmyk(r, g, b)
end

--[====[
	@type func
	@name drag(e)
	@param e:table Объект события
	@brief Перетаскивает объекты
	@description [
		Если на перетаскиваемый объект установить <b>obj.dragBorder = true</b> - будут соблюдаться границы экрана.
		Если на преетаскиваемый объект установить <b>callback(phase, target)</b> будет вызываться солбек.
	]
--]====]
function drag(e)
	local target = e.target

	local phase = e.phase

	if "began" == phase then
		local parent = target.parent
		parent:insert(target)
		display.getCurrentStage():setFocus(t)

		target.isFocus = true

		target.x0 = e.x - target.x
		target.y0 = e.y - target.y

		if not empty(target.callback) then
			target.callback(phase, target)
		end
	elseif target.isFocus then
		if "moved" == phase then
			local x = e.x - target.x0
			local y = e.y - target.y0

			if target.dragBorder then
				-- Проверяем X
				local leftBorder = target.width / 2
				local rightBorder = _W - target.width / 2

				if x < leftBorder then
					target.x = leftBorder
				elseif x > rightBorder then
					target.x = rightBorder
				else
					target.x = x
				end

				-- Проверяем Y
				local topBorder = target.height / 2
				local bottomBorder = _H - target.height / 2

				if y < topBorder then
					target.y = topBorder
				elseif y > bottomBorder then
					target.y = bottomBorder
				else
					target.y = y
				end
			else
				target.x = x
				target.y = y
			end

			if not empty(target.callback) then
				target.callback(phase, target)
			end
		elseif "ended" == phase or "cancelled" == phase then
			target.isFocus = false

			if not empty(target.callback) then
				target.callback(phase, target)
			end
		end
	end

	return true
end

--[====[
	@type func
	@name inRadius(current, target, radius)
	@param current:table отображаемый объект
	@param target:table конечный объект
	@param radius:number Числововй радиус
	@brief Определаяет, находится ли текущий объект в раидусе конечного объекта
--]====]
function inRadius(current, target, radius)
	local currX = current.x
	local currY = current.y
	local targetX = target.x
	local targetY = target.y

	if empty(currY) or empty(targetY) then
		return currX <= (targetX + radius) and currX >= (targetX - radius)
	elseif empty(currX) or empty(targetX) then
		return currY <= (targetY + radius) and currY >= (targetY - radius)
	else
		return currX <= (targetX + radius) and currX >= (targetX - radius) and
				currY <= (targetY + radius) and currY >= (targetY - radius)
	end
end

return M