---------------------------------------------------------------------------------
--
-- v_0.0.3
-- Вспомагательные ф-ии для математики
--
---------------------------------------------------------------------------------

local M = {}

--[====[
	@type func
	@name libMath.length{params}
	@param a:object Вектор A {x, y}
	@param b:object Вектор B {x, y}
	@return number
	@brief Находит длину вектора
	@description [
		Находит длину вектора. Принимает на вход векторы A и B
	]
--]====]
M.length = function(params)
	--	Весторы A&B
	local a = params.a
	local b = params.b

	local ab = {
		x = b.x - a.x,
		y = b.y - a.y
	}

	return math.sqrt(math.pow(ab.x, 2) + math.pow(ab.y, 2))
end

--[====[
	@type func
	@name libMath.hypLen{params}
	@param a:number Длина катета A
	@param b:number Длина катета B
	@return number
	@brief Находит длину гипотенузы
--]====]
M.hypLen = function(a, b)
	a = math.pow(a, 2)
	b = math.pow(b, 2)

	return math.sqrt(a + b);
end

return M
