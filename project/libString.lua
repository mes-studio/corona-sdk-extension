---------------------------------------------------------------------------------
--
-- v_0.0.3
-- libString.lua - расширение функциональности string
--
---------------------------------------------------------------------------------

--[====[
	@type func
	@name string.split(pString, pPattern)
	@param pString:string Входящая строка
	@param pPattern:string Шаблон для разделение строки
	@brief Разибавает строку на массив через разделитель
--]====]
string.split = function (pString, pPattern)
	if string.find(pString,".") then
		pString = string.gsub(pString,"%.","'.'")
	end

	if pPattern == "." then
		pPattern = "'.'"
	end

	local Table = {}  -- NOTE: use {n = 0} in Lua-5.0
	local fpat = "(.-)" .. pPattern
	local last_end = 1
	local s, e, cap = pString:find(fpat, 1)
	while s do
		if s ~= 1 or cap ~= "" then
			table.insert(Table,cap)
		end
		last_end = e+1
		s, e, cap = pString:find(fpat, last_end)
	end
	if last_end <= #pString then
		cap = pString:sub(last_end)
		table.insert(Table, cap)
	end

	return Table
end
