-------------------------------------------
--
-- v_0.0.3
-- Library libUrl
--
-------------------------------------------
--	Work with URL
--	Required libraries: libUtils(global)

local M = {}

--[====[
	@type func
	@name libUrl.encodeURI(str)
	@param str:string Input string
	@return string Encoded string
	@brief Encode input string. Similar function exist in JavaScript - encodeURI
--]====]
M.encodeURI = function(str)
  str = string.gsub(str, "\r?\n", "\r\n")

  str = string.gsub(str, "([^%w%-%.%_%~ ])",
	function(c) return string.format("%%%02X", string.byte(c)) end)

  --Convert spaces to plus signs
  str = string.gsub(str, " ", "%%20")

  return str
end

--[====[
	@type func
	@name libUrl.createQueryParams()
	@brief Create queryParams
	@description [
		You can use <b>libUrl.createQueryParams</b>, <b>libUrl.addQueryParam</b> and <b>libUrl.getQueryParams</b> 
		for get this result: <i>name=Jon&age=44</i>
	]
--]====]
M.createQueryParams = function()
	--	Init queryParams
	M.queryParams = {}
end

--[====[
	@type func
	@name libUrl.addQueryParam(naem, value)
	@param name:string Parameter name
	@param value:any Parameter value
	@brief Add new parameter to queryParams
--]====]
M.addQueryParam = function(name, value)
	M.queryParams[name] = value
end

--[====[
	@type func
	@name libUrl.getQueryParams(params?)
	@param params:table Optional. Can be use for get cusom query parameters
	@return string Query params string
	@brief Return queryParams like this "master=Stas&slave=Vlad"
	@description [
		Return queryParameters string. Optional <b>params</b> can be use if you have table and whant create query parameters string
		from this table. You don't should use <b>libUrl.addQueryParam</b> for create query parameters string
	]
--]====]
M.getQueryParams = function(params)
	if not empty(params) and not isTable(params) then
		return nil
	end

	if empty(params) then
		params = M.queryParams
	end

	local queryParamsStr = ''
	local separator = ''

	for paramKey, param in pairs(params) do
		queryParamsStr = queryParamsStr .. separator .. paramKey .. '=' .. param
		separator = '&'
	end

	return queryParamsStr
end

--[====[
	@type func
	@name libUrl.getQueryParamsObj()
	@return table queryParams table
	@brief Return queryParams
	@description [
		Return <b>queryParams</b> obj that have all added parameters
	]
--]====]
M.getQueryParamsObj = function()
	return M.queryParams 
end

--[====[
	@type func
	@name libUrl.createHeaders()
	@brief Create headers. Similar to create query parameters
--]====]
M.createHeaders = function()
	M.headers = {}
end

--[====[
	@type func
	@name libUrl.addHeader(name, value)
	@param name:string Header name
	@param value:any Header value
	@brief Add new header
--]====]
M.addHeader = function(name, value)
	M.headers[name] = value
end

--[====[
	@type func
	@name libUrl.getHeaders()
	@return table Headers table
	@brief Return headers object
--]====]
M.getHeaders = function()
	return M.headers
end

return M